#!/usr/bin/python3


import string
import os
import time


def show_error():
    print('Please choose an option between 1 and 4')


def main():
    CaesarCipher().run()


class CaesarCipher:
    offset = 0
    encryption_cipher = {}
    decryption_cipher = {}

    def _set_offset(self):
        self.clear_console()
        print('Current Offset: %s\n' % self.offset)
        self.offset = int(input('Choose a value for offset: '))
        self.encryption_cipher = self.create_cipher(self.offset)
        self.decryption_cipher = {v: k for k, v in self.encryption_cipher.items()}


    def encrypt_message(self, is_encrypting):
        self.clear_console()
        message_to_encrypt = input('Enter message to encrypt: ')
        cipher = self.encryption_cipher if is_encrypting else self.decryption_cipher

        encrypted_message = ''.join(cipher.get(x.upper(), x) for x in message_to_encrypt)

        print(encrypted_message)
        input('Press [ENTER] to continue')


    def show_menu(self):
        print()
        print('Current Offset: %s' % self.offset)
        print()
        print('1. Set offset')
        print('2. Encrypt message')
        print('3. Decrypt message')
        print('4. Self-destruct')


    def clear_console(self):
        os.system('clear')


    def show_menu_error(self):
        self.clear_console()
        print('Error: Please choose a value between 1 and 4.')
        input('Press [ENTER] to continue')


    def self_destruct(self):
        program_will_destruct = 'This program will self-destruct in %s...'
        self.clear_console()

        for x in reversed(range(1, 6)):
            print(program_will_destruct % x)
            time.sleep(1)
            self.clear_console()

        print('BOOM!')
        time.sleep(0.5)
        self.clear_console()
        exit(0)


    def run(self):
        while True:
            self.clear_console()
            self.show_menu()
            choice = input('Choose an option: ')

            {
                '1': self._set_offset,
                '2': lambda: self.encrypt_message(True),
                '3': lambda: self.encrypt_message(False),
                '4': self.self_destruct
            }.get(choice, self.show_menu_error)()


    def increment_letter(self, letter, offset):
        position = ord(letter.upper()) - ord('A')
        new_position = (position + offset) % len(string.ascii_uppercase)
        return string.ascii_uppercase[new_position]


    def create_cipher(self, offset):
        return {
            letter: self.increment_letter(letter, offset) for letter in string.ascii_uppercase
        }


if __name__ == "__main__":
    main()
